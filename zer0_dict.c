#include "zer0_dict.h"

#include <immintrin.h>
#include <string.h>

void
dict_init(zer0_dict *d, uint32_t c, size_t s) {
        d->cap  = c;
        d->len  = 0;
#ifdef _WIN32
        d->keys = _aligned_malloc(c * sizeof (int32_t), 32);
#else
        d->keys = aligned_alloc(32, c * sizeof (int32_t));
#endif
        d->vals = malloc(c * s);
}

void
dict_push(zer0_dict *d, int32_t k, void *v, size_t s) {
        if (d->len != d->cap) { // most common case
                d->keys[d->len] = k;

                void *p;
                p = (uint8_t*)d->vals + d->len * s;

#ifdef _WIN32
                memcpy_s(p, s, v, s);
#else
                memcpy(p, v, s);
#endif

                ++d->len; 
        }
}

void*
dict_find(zer0_dict *d, int32_t k, size_t s) {
        uint32_t i;
        i = 0;

        while (i < d->len) {
                if (d->keys[i] == k) {
                        void *p;
                        p = (uint8_t*)d->vals + i * s;

                        return p;
                }

                ++i;
        }

        return NULL;
}

void*
dict_find_simd(zer0_dict *d, int32_t k, size_t s) {
#ifdef __AVX2__
        __m256i ymm0;
        ymm0 = _mm256_broadcastd_epi32(*(__m128i*)&k);

        __m256i  ymm1;
        uint32_t i;
        int      m;

        i = 0;
        m = 0;
        while (i < d->len && m == 0) {
                ymm1 = _mm256_load_si256((__m256i*)(d->keys + i));
                ymm1 = _mm256_cmpeq_epi32(ymm1, ymm0);

                m = _mm256_movemask_epi8(ymm1);

                i += 8;
        }

        if (m == 0)
                return NULL;

        uint8_t b;
        b = _tzcnt_u32(m) >> 2;

        i -= 8;
        i += b;

        void *p;
        p = (uint8_t*)d->vals + i * s;

        return p
#else
	return NULL;
#endif
}

void
dict_free(zer0_dict *d) {
#ifdef _WIN32
        _aligned_free(d->keys);
#else
        free(d->keys);
#endif

        free(d->vals);
}
