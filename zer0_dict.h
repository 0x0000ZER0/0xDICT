#ifndef ZER0_DICT_H
#define ZER0_DICT_H

#include <stdint.h>
#include <stddef.h>

typedef struct {
        uint32_t  len;
        uint32_t  cap;
        int32_t  *keys;
        void     *vals;
} zer0_dict;

void
dict_init(zer0_dict *d, uint32_t c, size_t s);

void
dict_push(zer0_dict *d, int32_t k, void *v, size_t s);

void*
dict_find(zer0_dict *d, int32_t k, size_t s);

void*
dict_find_simd(zer0_dict *d, int32_t k, size_t s);

void
dict_free(zer0_dict *d);

#endif
